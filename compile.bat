@echo off

REM start clean every time
rmdir bin /S /Q
mkdir bin

REM copy over the natives that are required for the code to run
xcopy /s /q lib\natives bin

set classpath=lib\lwjgl-3.1.2\*;.
javac -d bin -cp %classpath% src\java\com\demo\*.java