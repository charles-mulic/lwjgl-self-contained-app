## LWJGL Self-Contained Application (Windows)

This project aims to be the bare minimum required to get a LWJGL Java application to build as a self-contained application. This means that the end user will not need Java installed on their machine, as your distribution will include its own private runtime.

## Building

Since the goal is to create a bare minimum, simple project, build tools have been avoided in favor of batch scripts running javapackager commands directly. This should work on your machine as long as the JDK is on your PATH. A full distribution of LWJGL 3.1.2 is included in lib, though only lwjgl.jar, lwjgl-glfw.jar, and lwjgl-opengl.jar are needed to run the demo.

From a command terminal in the base directory, you can run these scripts by typing their names:
```
build - this will clean, compile, build the jar, and build the native distribution(s)
compile - compile only
launch - this will launch the built jar from the console (useful for seeing any Java/LWJGL related problems)
```

## Running

After running the build command, the base directory will contain a dist folder, which contains a bundles folder. The bundles folder will contain a Game-1.0.exe installer, as well as a Game folder containg a Game.exe which can be run immediately.

## Requirements

In order to build the .exe, javapackager will require you to download Inno Setup (http://jrsoftware.org/isdl.php). You can attempt to build without this, but you will see this as being the suggested fix in the console. MSI has an additional requirement. See the console output.

## Other platforms

In order to get this working on other platforms, you'll need to adapt the contents of the batch scripts to corresponding scripts for your target platform. You'll also need to acquire a LWJGL distribution that includes the natives for your platform, and extract the necessary natives. The compile batch script copies the native files from lib/natives into the bin folder so that they are included in the jar. There may be other dependencies as well that javapackager should tell you about when running the -deploy -native.

## See also

https://gitlab.com/charles-mulic/lwjgl-self-contained-app-gradle - a Gradle implementation with a custom task for building the native distribution(s)