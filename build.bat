@echo off

call compile.bat

REM start clean every time
rmdir build /S /Q
rmdir dist /S /Q
mkdir build

set appclass=com.demo.Main
set srcdir=bin
set jarFileName=build\Game
set classpath=.;lwjgl.jar;lwjgl-glfw.jar;lwjgl-opengl.jar

REM copying over the necessary jars, you could just include everything from lwjgl instead
xcopy /s /q lib\lwjgl-3.1.2\lwjgl.jar build
xcopy /s /q lib\lwjgl-3.1.2\lwjgl-glfw.jar build
xcopy /s /q lib\lwjgl-3.1.2\lwjgl-opengl.jar build

REM creates our jar file from the contents of .\bin created by compile.bat
javapackager -createjar -appclass %appclass% -srcdir %srcdir% -outfile %jarFileName% -v -classpath %classpath%

set outputDirectory=dist
set outputFileName=GameTest
set deploySrcDir=build
set srcfiles=Game.jar
set name="Game"
set title="Game Demo"

REM creates our native application(s) from our jar file
javapackager -deploy -native -outdir %outputDirectory% -outfile %outputFileName% -srcdir %deploySrcDir% -appclass %appclass% -name %name% -title %title%